export default class Main extends Phaser.State {
    constructor() {
        super();
    }
    create() {
        this.game.hp = 12;
        this.game.level = 1;
        this.game.createMask();
        this.game.createBackground(2);
        this.game.createWalls();
        this.game.createHealthbar();
        this.game.createLevelText();
        this.game.createPlatforms();
        this.game.createCeiling();
        this.game.createModes();
        this.game.createRecord();
        this.createButtons();
        this.createPlayer();
        this.game.counter = 0;
        this.cursor = this.input.keyboard.createCursorKeys();
    }
    createButtons() {
        this.pauseButton = this.add.button(512, 345, 'button', () => {
            this.game.paused = !this.game.paused;
        }, this);
        this.abortButton = this.add.button(512, 405, 'button', () => {
            if (this.game.paused) {
                this.game.paused = !this.game.paused;
            }
            if (this.game.level > this.game.record) {
                this.game.record = this.game.level;
            }
            this.inputName();
            this.state.start('Menu');
        }, this);
    }
    createPlayer() {
        this.player = this.add.sprite(235, 434, 'player');
        this.player.mask = this.game.mask;
        this.player.anchor.set(0.5);
        this.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 1000;
        this.player.animations.add('left', [0, 1, 2, 3], 24);
        this.player.animations.add('right', [9, 10, 11, 12], 24);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 24);
        this.player.animations.add('flyright', [27, 28, 29, 30], 24);
        this.player.animations.add('fly', [36, 37, 38, 39], 24);
    }
    update() {
        this.game.updateWall();
        this.game.updateTexture();
        this.game.updateHealthbar();
        this.game.updatePlatforms();
        this.game.updateDigitAnimation();

        if (this.player.y < 137) {
            if (this.player.touching.body.enable) {
                this.player.touching.body.enable = false;
                this.game.hp -= 5;
                if (this.player.body.velocity.y < 0) {
                    this.player.body.velocity.y = 0;
                }
            }
        }
        this.physics.arcade.collide(this.player, this.game.platforms, this.onTouchPlatform.bind(this));
        this.physics.arcade.collide(this.player, this.game.walls);

        this.updateCursor();
        this.updatePlayerAnimation();

        this.game.platforms.forEach(platform => {
            if (platform.y < this.player.y - 20) {
                platform.body.enable = false;
            }
        }, this);

        if (this.player.y > 475 || this.game.hp <= 0) {
            if (this.game.hp <= 0) {
                this.game.hp = 0;
            }
            if (this.game.level > this.game.record) {
                this.game.record = this.game.level;
            }
            this.inputName();
            this.state.start('Menu');
        }
    }
    updateCursor() {
        const pointer = this.input.activePointer;
        if (this.cursor.left.isDown || 
            (pointer.isDown && pointer.worldX < 233)) {
            this.player.body.velocity.x = -200;
        } else if (this.cursor.right.isDown || 
            (pointer.isDown && pointer.worldX > 233 && pointer.worldX < 442)) {
            this.player.body.velocity.x = 200;
        } else {
            this.player.body.velocity.x = 0;
        }
    }
    updatePlayerAnimation() {
        const x = this.player.body.velocity.x;
        const y = this.player.body.velocity.y;
        if (!x && !y) {
            this.player.frame = 8;
        } else if (!x) {
            this.player.animations.play('fly');
        } else if (!y) {
            x < 0 ? this.player.animations.play('left') : this.player.animations.play('right');
        } else {
            x < 0 ? this.player.animations.play('flyleft') : this.player.animations.play('flyright');
        }
    }
    onTouchPlatform(player, platform) {
        if (platform.key === 'conveyorRight') {
            if (player.touching !== platform) {
                if (this.game.hp < 12) {
                    ++this.game.hp;
                }
            }
            this.player.x += 2;
        } else if (platform.key === 'conveyorLeft') {
            if (player.touching !== platform) {
                if (this.game.hp < 12) {
                    ++this.game.hp;
                }
            }
            this.player.x -= 2;
        } else if (platform.key === 'trampoline') {
            if (this.game.hp < 12) {
                ++this.game.hp;
            }
            platform.animations.play('jump');
            player.body.velocity.y = -350;
        } else if (platform.key === 'nails') {
            if (player.touching !== platform && player.body.facing === Phaser.DOWN) {
                this.game.hp -= 5;
            }
        } else if (platform.key === 'normal') {
            if (player.touching !== platform) {
                if (this.game.hp < 12) {
                    ++this.game.hp;
                }
            }
        } else if (platform.key === 'fake') {
            if (player.touching !== platform) {
                if (this.game.hp < 12) {
                    ++this.game.hp;
                }
                platform.animations.play('turn');
                setTimeout(() => {
                    platform.body.enable = false;
                }, 100);
            }
        }
        player.touching = platform;
    }
    inputName() {
        do {
            if (this.game.name === null) {
                this.game.name = undefined;
            }
            this.game.name = window.prompt(`您到達了地下 ${this.game.level} 階！請輸入您的大名：`, this.game.name);
        } while (!this.game.name);

        firebase.database().ref('/board').push({
            name: this.game.name,
            level: this.game.level
        });
    }
}